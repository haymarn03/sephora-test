angular.module('Sephora').controller('ProductController', function($scope, $stateParams, $location, DataService, ProductService) {

  $scope.init = function(){
    
    $scope.filters = $scope.pagination = $scope.sort_range = $scope.products = [];
    // selected filters
    $scope.price_range = '';
    $scope.categories = [];
    $scope.page_size = '';
    $scope.page_number = 1;
    $scope.page_range = [];
    $scope.sort = '';
    $scope.under_sale = false;
    $scope.total_count = 0;
    $scope.custom_filters = '';

    // get filterable data
    DataService.filters().then(function(data) {
      $scope.filters = data.filters;
      $scope.pagination = data.page;
      $scope.sort_range = data.sort;

      // set default sort & pagination
      $scope.sort = $scope.sort_range[0].uniq_name;
      $scope.page_size = $scope.pagination[0];
    });

    if ($stateParams.category != undefined){
      custom_filters = [];
      $scope.custom_filters += "filter[category_eq]=" + $stateParams.category;
    }

    // get products list
    getProductList($scope.custom_filters);
  }
  $scope.init();
  
  $scope.callToFilter = function(){
    $scope.custom_filters = '';

    // set categories filter
    if($scope.categories.selected != null){
      var selected = [];
      angular.forEach($scope.categories.selected, function(value, key){
        if(value == true)
          selected.push(key); 
      });

      if(selected.length != 0){
        selected.sort();
      }

      $scope.selected_categories = selected.join(',');
      if (selected.length == 1){
        $scope.custom_filters += $scope.custom_filters != '' ? '&' : ''
        $scope.custom_filters += "filter[category_eq]=" + $scope.selected_categories;
      }
      else if (selected.length > 1){
        $scope.custom_filters += $scope.custom_filters != '' ? '&' : ''
        $scope.custom_filters += "filter[category_in]=" + $scope.selected_categories;
      }
    }

    // set price filter
    if($scope.price_range != ''){
      // $scope.filter.push({'key': $scope.price_range.range, 'value': $scope.price_range.price});
      $scope.custom_filters += $scope.custom_filters != '' ? '&' : ''
      $scope.custom_filters += "filter[" + $scope.price_range.range+"]=" + $scope.price_range.price;
    }

    // set sort filter
    if($scope.sort != ''){
      $scope.custom_filters += $scope.custom_filters != '' ? '&' : ''
      $scope.custom_filters += "sort="+ $scope.sort;
    }
    
     // set pagination
    if($scope.page_size != ''){
      $scope.custom_filters += $scope.custom_filters != '' ? '&' : ''
      $scope.custom_filters += "page[size]="+ $scope.page_size;
    }
    if($scope.page_number != ''){
      $scope.custom_filters += $scope.custom_filters != '' ? '&' : ''
      $scope.custom_filters += "page[number]="+ $scope.page_number;
    }

    // // get products list
    getProductList($scope.custom_filters);
  }

  $scope.goToPage = function(e){
    e.preventDefault();
    $('.pagination > li > a').removeClass('active');
    
    var childText = e.target.innerText;
    $scope.page_number = childText;
    $(e.target).addClass('active');
    $scope.callToFilter();
  } 

  $scope.clearFilters = function(){
    $scope.categories = [];
    $scope.price_range = '';
    $scope.callToFilter();
  }

  function getProductList(custom_filters){
    ProductService.products(custom_filters).then(function(data) {
      $scope.products = data.data;
      $scope.total_count = $scope.products.length;
      set_pagination($scope.total_count);
    });
  }

  function getRelatedProductList(category_filter){
    ProductService.products(category_filter).then(function(data) {
      $scope.related_products = data.data;
      console.log("related products");
      console.log($scope.related_products);
    });
  }

  function set_pagination(total_count){
    if($scope.total_count != 0){
      var num = Math.ceil(total_count / $scope.page_size.toFixed(2));
      for(i = 1; i <= num; i++){
        $scope.page_range.push(i);
      }
    }
  }

  // Product detail
  if ($stateParams.id != undefined){
    $scope.product_id = $stateParams.id;
    ProductService.product_detail($scope.product_id).then(function(data) {
      $scope.product = data.data;
      console.log("product detail");
      console.log($scope.product);

      // get related products
      $scope.category += "filter[category_eq]=" + $scope.product.attributes.category;
      getRelatedProductList($scope.category);
    });
  }
 
});
  
