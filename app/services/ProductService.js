angular.module('Sephora').service('ProductService', function(ProductFactory) {
  
  var ProductService = {

    products: function(filter) {
      var data = ProductFactory.get_all(filter)
        .then(function successCallback(response) {
          return response.data;
        }).catch(function(err) {
          return err;
        });
      return data;
    },
    product_detail: function(id) {
      var data = ProductFactory.get_detail(id)
        .then(function successCallback(response) {
          return response.data;
        }).catch(function(err) {
          return err;
        });
      return data;
    }
    
  };
  return ProductService;

});
  
