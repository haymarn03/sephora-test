angular.module("Sephora").factory("ProductFactory", function($http, BaseAPI) {

  var Product = [];
  
  Product.get_all = function(filter){
    return $http({
      method : "GET",
      url : BaseAPI + "/products?" + filter
    });
  }

  Product.get_detail = function(id){
    return $http({
      method : "GET",
      url : BaseAPI + "/products/" + id
    });
  }

  return Product;
});